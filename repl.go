package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
)

type Repl struct {
	e  Evaluator
	in *bufio.Reader
}

type Evaluator interface {
	parse(string) (interface{}, bool)
	eval(interface{}) interface{}
	print(interface{}) string
}

func (r *Repl) loop() {
	r.in = bufio.NewReader(os.Stdin)
	for {
		exp, err := r.read()
		if err != nil {
			panic("read() failed" + err.Error())
		}
		result := r.e.eval(exp)
		r.display(r.e.print(result))
	}
}

func (r *Repl) read() (interface{}, error) {
	var input bytes.Buffer
	first := true
	for {
		if first {
			fmt.Print("> ")
			first = false
		} else {
			fmt.Print("   ")
		}
		line, isPrefix, err := r.in.ReadLine()
		if TRACE {
			fmt.Printf("Readline() -> %v,%v,%v\n", string(line), isPrefix, err)
		}
		if err != nil {
			return nil, err
		}
		input.Write(line)
		input.WriteString("\n")
		if isPrefix {
			continue
		}
		exp, ok := r.e.parse(input.String())
		if ok {
			return exp, nil
		}
	}
	panic("should not reach here")
}

func (r *Repl) display(s string) {
	fmt.Printf("=> %s\n", s)
}
