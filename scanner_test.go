package main

import (
	"strings"
	"testing"
)

func TestScanPlusSign(t *testing.T) {
	var s Scanner
	s.Init(strings.NewReader("+"))
	tok := s.Scan()
	if tok != Ident {
		t.Errorf("+ is an identifer, but %v, %v, %v", tok, TokenString(tok), s.TokenText())
	}
}

func TestScanCommentsBeginningSemiColon(t *testing.T) {
	var s Scanner
	s.Init(strings.NewReader("T ;; abc \n ;\n F"))
	tok := s.Scan()
	if tok != Ident || s.TokenText() != "T" {
		t.Errorf("+ is an identifer, but %v, %v, %v", tok, TokenString(tok), s.TokenText())
	}
	tok = s.Scan()
	if tok != Ident || s.TokenText() != "F" {
		t.Errorf("+ is an identifer, but %v, %v, %v", tok, TokenString(tok), s.TokenText())
	}
}

func TestDotAsToken(t *testing.T) {
	var s Scanner
	s.Init(strings.NewReader("(a . b)"))

	var toks []rune
	for {
		tok := s.Scan()
		if tok == EOF {
			break
		}
		toks = append(toks, tok)
	}

	expected := []rune{'(', Ident, '.', Ident, ')'}

	if print(toks) != print(expected) {
		t.Errorf("%v != %v", toks, expected)
	}
}
