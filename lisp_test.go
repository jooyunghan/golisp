package main

import "testing"

type testInput struct {
	exp    string
	result string
	env    map[string]expression
}

var evalTests = []testInput{
	{"T", "T", defaultEnv()},
	{"NIL", "()", defaultEnv()},
	{"(quote a)", "a", nil},
	{"(car (quote (a b)))", "a", nil},
	{"(cdr (quote (a b c)))", "(b c)", nil},
	{"(cdr (quote (a)))", "()", nil},
	{"(cons (quote a) (quote (b c)))", "(a b c)", nil},
	{"(equal (car (quote (a b))) (quote a))", "T", nil},
	{"(atom (quote a))", "T", nil},
	{"(atom (quote ()))", "T", nil},
	{"(atom NIL)", "T", defaultEnv()},
	{"(atom (quote (a)))", "()", nil},
	{"(cond ((atom (quote a)) (quote b)) ((quote T) (quote c)))", "b", nil},
	{"a", `"b"`, map[string]expression{"a": "b"}},
	{"((lambda (x y) (cons (car x) y)) (quote (a b)) (cdr (quote (c d))))", "(a d)", nil},
	{"((label ff (lambda (x) (cond ((atom x) x) ((quote T) (ff (car x)))))) (quote ((a b) c)))", "a", nil},
	{"(list (quote a) (quote b))", "(a b)", nil},
	{"(list)", "()", nil},
	{"(null NIL)", "T", defaultEnv()},
	{"(null (quote ()))", "T", defaultEnv()},
	{"(null (quote a))", "()", defaultEnv()},
	{"(null (quote (a l)))", "()", defaultEnv()},
	{"(cadr (quote (a b)))", "b", defaultEnv()},
	{"(or)", "()", defaultEnv()},
	{"(or (atom (quote t)))", "T", defaultEnv()},
	{"(or (atom (cons (quote a) (quote b))) (atom (quote t)))", "T", defaultEnv()},
	{"(or (atom (cons (quote a) (quote b))) (equal T NIL))", "()", defaultEnv()},
	{"(and)", "T", defaultEnv()},
	{"(and (atom (quote t)))", "T", defaultEnv()},
	{"(and (atom (cons (quote a) (quote b))) (atom (quote t)))", "()", defaultEnv()},
	{"(and (atom (quote c)) (atom (quote t)))", "T", defaultEnv()},
	{"(not T)", "()", defaultEnv()},
	{"(not (or T))", "()", defaultEnv()},
	{"(not (and T NIL))", "T", defaultEnv()},
	{"\"test\"", "\"test\"", defaultEnv()},
	{"'a", "a", defaultEnv()},
	{"(+)", "0", defaultEnv()},
	{"(+ 1 2)", "3", defaultEnv()},
	{"(+ 1 2.1)", "3.1", defaultEnv()},
	{"(+ 1 (+ 2 3))", "6", defaultEnv()},
	{"(+ 1 2 3)", "6", defaultEnv()},
	{"(- 10 3)", "7", defaultEnv()},
	{"(* 10 3)", "30", defaultEnv()},
	{"(/ 10 2)", "5", defaultEnv()},
	{"(/ 10 2 5)", "1", defaultEnv()},
	{"(/ 10)", "0.1", defaultEnv()},
	{"(= 10 10)", "T", defaultEnv()},
	{"(= 10 10 10)", "T", defaultEnv()},
	{"(= 10 10 11)", "()", defaultEnv()},
	{"(maplist '(1 2 3) (function (lambda (l) (* 2 (car l)))))", "(2 4 6)", defaultEnv()},
}

var loadTests = []testInput{
	{"(defun cc (x y) (cons x y)) (cc (quote a) (quote b))", "(a . b)", defaultEnv()},
	{"(defun alt (x) (cond ((or (null x) (null (cdr x))) x) (T (cons (car x) (alt (cddr x)))))) (alt (quote (a b c d e)))", "(a c e)", defaultEnv()},
	{`
(defun vfoo x x) 
(vfoo 1 2 3)`, "(1 2 3)", defaultEnv()},
	{`
(defun vfoo (a . x) x) 
(vfoo 1 2 3)`, "(2 3)", defaultEnv()},
}

func TestEvalString(t *testing.T) {
	for i, tt := range evalTests {
		env := tt.env
		if env == nil {
			env = defaultEnv()
		}
		if result := evalString(tt.exp, env); result != tt.result {
			t.Errorf("#%d: eval failed: %s (expected:%s, actual:%s)", i, tt.exp, tt.result, result)
			continue
		}
	}
}

func TestLoadString(t *testing.T) {
	for i, tt := range loadTests {
		if result := loadString(tt.exp, tt.env); result != tt.result {
			t.Errorf("#%d: eval failed: %s (expected:%s, actual:%s)", i, tt.exp, tt.result, result)
			continue
		}
	}
}

func TestEvalEmptyListShouldFail(t *testing.T) {
	defer func() { recover() }()
	eval(nil, defaultEnv())
	t.Error("eval of nil should fail")
}

func TestList(t *testing.T) {
	if "(1 2 3)" != print(list(1, 2, 3)) {
		t.Errorf("list(1,2,3) => %s", print(list(1, 2, 3)))
	}
}

func TestIsCxr(t *testing.T) {
	tests := []string{"car", "cdr", "cadr", "cadar"}
	for _, s := range tests {
		if !isCxr(Symbol(s)) {
			t.Errorf("%s is cxr", s)
		}
	}
}

func TestIsNotCxr(t *testing.T) {
	tests := []string{" car", "cdra", "cr", "dar"}
	for _, s := range tests {
		if isCxr(Symbol(s)) {
			t.Errorf("%s is not cxr", s)
		}
	}
}

func BenchmarkRecursion(b *testing.B) {
	env := defaultEnv()
	exp := parse("((label sum (lambda (n) (cond ((= 0 n) 0) (T (+ n (sum (- n 1))))))) 1000)")
	for i := 0; i < b.N; i++ {
		eval(exp, env)
	}
}
