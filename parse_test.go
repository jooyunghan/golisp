package main

import (
	"testing"
)

var parseTests = []string{
	"a",
	`"a"`,
	"(quote a)",
	"()",
	"(() ())",
	"(cdr (quote (a)))",
	"(a . b)",
}

func TestParse(t *testing.T) {
	for i, e := range parseTests {
		p, ok := Parse(e)
		if !ok {
			t.Errorf("#%d: parse failed: input:%v, error:%v", i, e, p)
			continue
		}
		if pp := print(p); e != pp {
			t.Errorf("#%d: parse-print failed: input:%v, parse-printed:%v]", i, e, pp)
			continue
		}
	}
}

func TestPartialInput(t *testing.T) {
	shouldBeEqual := func(a, b interface{}) {
		if a != b {
			t.Errorf("%v != %v", a, b)
		}
	}

	e, ok := Parse("(car")
	shouldBeEqual(e, nil)
	shouldBeEqual(ok, false)
}

func TestQuote(t *testing.T) {
	shouldBeEqual := func(a, b interface{}) {
		if a != b {
			t.Errorf("%v != %v", a, b)
		}
	}

	e, _ := Parse("'a")

	shouldBeEqual(car(e), Symbol("quote"))
	shouldBeEqual(cadr(e), Symbol("a"))
}

func TestParseWithNonLetterIdentifer(t *testing.T) {
	shouldBeEqual := func(a, b interface{}) {
		if a != b {
			t.Errorf("%v != %v", a, b)
		}
	}

	e, ok := Parse("(+ 1 2)")
	shouldBeEqual(ok, true)
	shouldBeEqual(car(e), Symbol("+"))
	shouldBeEqual(cadr(e), 1)
	shouldBeEqual(caddr(e), 2)
}

func TestCons(t *testing.T) {
	shouldBeEqual := func(a, b interface{}) {
		if a != b {
			t.Errorf("%v != %v", a, b)
		}
	}

	e, ok := Parse("(a . b)")
	shouldBeEqual(ok, true)
	shouldBeEqual(car(e), Symbol("a"))
	shouldBeEqual(cdr(e), Symbol("b"))
}
